# OpenBSD 6.8 - Installation Notes (revised 7.5)
------------------------------------------------

# A drafted installation process for OpenBSD (including WiFi, Gnome, ports and more)
Some content might be altered (revised 7.5) to get a proper installation.
This is a draft/research document containing command lines (usage for research purpose only).

[install process](https://gitlab.com/mrosner/2021-research-os-OpenBSD/blob/main/installation-process-for-OpenBSD.md)

## Preparing an installation media (USB stick)
The following steps are showing how to download and create a bootable usb stick. A General check for architecture and boot media are required (for this example the architecture is amd64 and installation media is a USB flash drive). Proceed to download an appropriate image (install68.img) and the SHA256, SHA256.sig files for later integrity checks.
### Variant A - using curl
```
user $ curl -o install68.img https://cdn.openbsd.org/pub/OpenBSD/6.8/amd64/install68.img
user $ curl -o SHA256 https://cdn.openbsd.org/pub/OpenBSD/6.8/amd64/SHA256
```
### Variant B - using wget
```
user $ wget https://cdn.openbsd.org/pub/OpenBSD/6.8/amd64/install68.img
user $ wget https://cdn.openbsd.org/pub/OpenBSD/6.8/amd64/SHA256
```
### Check for integrity
The SHA256 file contains the checksums for all the installation files. Make use of sha256 / sha256sum depending on the OS of choice.
#### Variant C - using BSD
```
user $ sha256 -C SHA256 install*.img
(SHA256) installXX.img: OK
```
#### Variant D - using GNU coreutils (Linux)
```
user $ sha256sum -c --ignore-missing SHA256
installXX.img: OK
```

### Verify and check for accidental corruption (OpenBSD)
```
$ signify -Cp /etc/signify/openbsd-XX-base.pub -x SHA256.sig install*.img
Signature Verified
installXX.img: OK
```

### Identify and create the USB media
#### Variant E (using disklabel - OpenBSD)
To identify the media the disklabel command is used.
```
root $ disklabel sd{0,1,2,3,4}
```
Being sure about the device file is essential to proceed with diskdump by using the identified device and the dd command.
```
root $ dd if=/path/to/install*.img of=/dev/rsd{0,1,2,3,4}c bs=1m
```
#### Variant E (using cfdisk - GNU/Linux)
To identify the media the cfdisk or fdisk command can be used.
```
root $ cfdisk /dev/sd{a,b,c,d,e}
```
Being sure about the device file is essential to proceed with diskdump by using the identified device and the dd command.
```
root $ dd if=/path/to/install*.img of=/dev/sd{a,b,c,d,e} bs=1m
```
After running the diskdump without failures, the installation USB media is ready to boot from.

## Booting from created media
By plugging in the USB stick the device will startup using OpenBSD.
### Variant F (create a fully encrypted drive)
To create a fully encrypted drive proceed with section [RAID and Disk Encryption](https://www.openbsd.org/faq/faq14.html) from [OpenBSD](https://www.openbsd.org/) directly.
### Variant G (make use of fully encrypted drive)
Sometimes there is a fully encrypted drive present already. To make use of such a drive create device nodes first (make sure to create some more in addition to further usage; depending on the number of disk drives). Using the (S) shell option will lead to (one might want to use dmsg to gather information on disk drives).
#### Creating a clean drive and device nodes in dev
Creating the missing device nodes is done by running the following (four nodes are created for later purpose).

Select (S) to execute a shell.

```
Welcome to the OpenBSD/amd64 7.4 installation program.
(I)nstall, (U)pgrade, (A)utoinstall or (S)hell? S
```

##### For number of disks accordingly create device nodes.

Create device nodes for drives.

```
root $ cd /dev
root $ sh MAKEDEV sd0
root $ sh MAKEDEV sd1
root $ sh MAKEDEV sd2
root $ sh MAKEDEV sd3
```

##### Check using dmesg
```
root $ dmesg
```

##### Drive overview using disklabel (00)

To get an overview use the disklabel command.

```
root $ disklabel sd{0,1,2,3}
```

##### Clear partition scheme
```
root $ dd if=/dev/zero of=/dev/rsd{0,1,2,3}c bs=1 count=256
```

##### Write random values to disk.

```
root $ dd if=/dev/urandom of=/dev/rsd{0,1,2,3}c bs=1m
```

##### Driver overview using disklabel (01)

```
root $ disklabel sd{0,1,2,3}
```

##### Blank

```
root $ fdisk -iy sd{0,1,2,3}                                            # (MBR)
```

```
root $ fdisk -gy -b 532480 sd{0,1,2,3}                                  # (GPT)
```


##### Edit using disklabel
```
root $ disklabel -E sd{0,1,2,3}                                         # (a, RAID, w, x)
root $ Label editor (enter '?' for help at any prompt)
sd0> a a
offset: [64]
size: [123456789] *
FS type: [4.2BSD] RAID
sd0*> w
sd0> q
No label changes.
```

##### Check partition using disklabel
```
root $ disklabel sd{0,1,2,3}
```

##### Create encrypted drive
```
root $ bioctl -c C -l sd{0,1,2,3}a softraid0                            # creating partition for install (mostly sd2)
```

##### Fix partition issue
```
root $ dd if=/dev/zero of=/dev/rsd{0,1,2,3}c bs=1m count=1              # (frequently 2)
```

##### Change passwd
```
root $ bioctl -P sd{0,1,2,3}
```

##### Detach drive
```
root $ bioctl -d sd{0,1,2,3}
```

#### Unlock the disk
Using disklabel will help to identify the drive. Unlocking the disk will then be done using the bioctl command and the correctly proviced passphrase or key instead - this example deals with a passphrase.
```
root $ disklabel sd{0,1,2,3}			# use one integer to identify the drive
root $ bioctl -c C -l sd{0,1,2,3} softraid0	# use the identified integer for suited drive
```
After unlocking the drive a sdX will be assigned to the drive as new device node. Unlocking the drive is complete. Remembering the assigned sdX will help in the further installation process. Leaving the shell is done with the exit command.
```
root $ exit
```
## Installation
### Partitions - alter space for /usr/obj
select (W)hole
select (E)edit

Alter drive to gain more space for objects.

```
sd2> d k
sd2> d j
sd2*> a j
offset: [200106208]
size: [268770512] 70GB
FS type: [4.2BSD]
mount point: [none] /usr/obj
sd2*> a k
offset: [346907584]
size: [121969136]
FS type: [4.2BSD]
mount point: [none] /home
sd2*> w
sd2> q
```

Proceeding with the installation is not that hard. Making good choices is supported by several sources - details in:
+ [OpenBSD 5.5 Installation Notes](https://ijsbeer.org:81/openbsd-55-installation-notes.html)
+ [OpenBSD 6.8 FAQ - Installation Guide](https://www.openbsd.org/faq/faq4.html) 
To succeed with the installation process it is important to remember the sdX {sd0,sd1,sd2,sd3,..} from the section above (unlock the disk).
After the installation has finished a (R) reboot will assure a first login to the new system - remembering the root / user's passphrase.
+ [OpenBSD 7.5 FAQ - Installation Guide](https://www.openbsd.org/faq/faq14.html#softraid)

### Update the system with firmware and patches
Updating the firmware and providing patches will be quickly achieved by running this commandline as root user.
```
root $ fw_update && syspatch && reboot
```
A reboot is neccessary - and booting into the updated system will be done; so new login is next.

### WiFi
To check for current network devices using dmesg is a good choice.
```
root $ dmesg | grep 'iwn0'

# etc.

root $ dmesg | grep 'rtwn0'
```
Adding a network will be done with an echo as root.
```
root $ echo 'join THE-ESSID wpakey WITH-THIS-KEY' >> /etc/hostname.iwn0
root $ chmod 640 /etc/hostname.iwn0
root $ sh /etc/netstart
```
In a good environment, performing these steps will lead to a wireless connection.

## Search and query packages
```
root $ pkg_info -Q gnome | grep extras                                  # Query for gnome-extras using -Q
```

## Adding Gnome
```
root $ pkg_add gnome gnome-extra gdm chromium nano
```

### Set memory demand accordingly (i.e. 8192MB) for login.conf
```
root $ nano /etc/login.conf
```

```
gnome:\
	:datasize-cur=1024M:\
	:tc=default:

gdm:\
	:tc=xenodm:
```

```
root $ groupadd -v -g 999 gnome
root $ usermod -G gnome ${username}
```

Having a GUI is nice! - by adding these packages, the Gnome environment will be installed.
```
root $ pkg_add gnome gnome-extra gdm chromium nano
root $ rcctl disable xenodm
root $ rcctl enable multicast messagebus avahi_daemon gdm
# root $ usermod -G staff USER
root $ rcctl start messagebus
root $ sysupgrade -s
```
And the USER be added to the staff group. By altering the login.conf the memory limits can be changed individually to each machines memory.
```
root $ nano /etc/login.conf	# edit staff section > datasize-cur = 8192; maxproc-max = 2048; maxproc-cur = 1024
```
## Power management
Having the lid close leading to suspension is great.
```
root $ echo 'apmd_flags="-A -z 10"' >> /etc/rc.conf.local	# suspend with 10% battery power
root $ echo 'machdep.lidaction=1' >> /etc/sysctl.conf		# 1 for suspend, 2 for hibernate
root $ rcctl start apmd
```

## Disable kernel modules

```
boot> boot -c
```

disable acpi and/or apm exit.

```
UKC> disable acpi
UKC> disable apm
UKC> exit
```

## Ports
Binary packages are great - sometimes building packages is prefered. To get the ports for the current release running will work.
```
user $ cd /tmp
user $ ftp https://cdn.openbsd.org/pub/OpenBSD/$(uname -r)/{ports.tar.gz,SHA256.sig}
user $ signify -Cp /etc/signify/openbsd-$(uname -r | cut -c 1,3)-base.pub -x SHA256.sig ports.tar.gz
```
After fetching the tar package changing to root user is needed to unpack the ports tree to the /usr directory.
```
root $ cd /usr
root $ tar xzf /tmp/ports.tar.gz
```
Initially changing the /etc/mk.conf might be desired to build packages.
```
root $ echo 'WRKOBJDIR=/usr/obj/ports' >> /etc/mk.conf
root $ echo 'DISTDIR=/usr/distfiles' >> /etc/mk.conf
root $ echo 'PACKAGE_REPOSITORY=/usr/packages' >> /etc/mk.conf
root $ cat /etc/mk.conf
```
Now it should be possible to build packages using ports.

After adding the portslist package -
```
root $ cd /usr/ports
root $ pkg_add portslist
```
searching ports will work as the following shows.
```
root $ make search key=gcc
```

## Build gcc
To build and install the GNU Compiler Collection changing the directory and some tea is required.
```
root $ cd /usr/ports/lang/gcc
root $ make
root $ make install
```
After the compilation has finished - the software will be installed to directories accordingly; gcc, g++, gfortran - check [GCC](https://gcc.gnu.org/wiki). 

## Miscellaneous
### Boot different kernel images
```
boot> ls
.
.
boot> bsd.sp
```

### Alter boot parameters
```
boot> boot -c
UKC> disable acpi
UKC> quit
```

### Copy and paste XTerm

```
user $ echo 'XTerm*selectToClipboard: true' >> ~/.Xresources
```

### Enable apmd
```
root $ rcctl enable apmd 
root $ rcctl set apmd flags -L
root $ rcctl start apmd
apmd(ok)
```

### Enable obsdfreqd
```
root $ rcctl enable obsdfreqd
root $ rcctl start obsdfreqd 
obsdfreqd(ok)
```

### Common packages
```
root $ pkg_add -u
root $ pkg_add nano doas chromium thunderbird
```

### Configure doas (Variant A)
```
root $ cp /etc/examples/doas.conf /etc/
root $ cat /etc/doas.conf
```

### Configure doas (Variant B)
```
root $ echo 'permit persist :wheel' >> /etc/doas.conf
root $ chmod 400 /etc/doas.conf
```

### Power management
Install package obsdfreqd.
```
root $ pkg_add obsdfreqd 
```

Set apmd flags, restart apmd, enable and start obsdfreqd
```
root $ rcctl ls on | grep ^apmd
root $ rcctl set apmd flags -L
root $ rcctl restart apmd
root $ rcctl enable obsdfreqd
root $ rcctl start obsdfreqd
```

## Refrences
Thanks for your visit!
If you like this document, consider to share it.

+ [OpenBSD - FAQ Installation Guide](https://www.openbsd.org/faq/faq4.html)
+ [OpenBSD - RAID and Disk Encryption](https://www.openbsd.org/faq/faq14.html)
+ [OpenBSD - manual page server](https://man.openbsd.org/man)
+ [OpenBSD on wikibooks](https://de.wikibooks.org/wiki/OpenBSD/_Systemprogramme)
+ [Keith Burnett - Running OpenBSD 6.8 on your laptop is really hard (not)](https://sohcahtoa.org.uk/openbsd.html)
+ [Rob Crowther - OpenBSD 5.5 Installation Notes](https://ijsbeer.org:81/tag/openbsd.html)
+ [angryfirelord - LinuxQuestions.org](https://www.linuxquestions.org/questions/*bsd-17/new-openbsd-install-freezes-4175501651/)
+ [Scott Bonds - marc.info](https://marc.info/?l=openbsd-tech&m=138867358707832&w=2)
+ [Solène Rapenne - dataswamp.org](https://dataswamp.org/~solene/2022-04-21-openbsd-71-fan-noise-temperature.html)

## Further references
+ [A](https://sohcahtoa.org.uk/openbsd.html)
+ [B](https://bwiggs.com/posts/2020-07-25-openbsd/)
+ [C](https://www.coreystephan.com/openbsd-thinkpad/#choice)

## Time stamps
+ 2023-12-15T18-36-58Z M.E.Rosner
+ 2023-12-16T13-25-31Z M.E.Rosner
+ 2024-05-30T18-09-25Z M.E.Rosner
