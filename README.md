# research os OpenBSD
---------------------
+ [install process](https://gitlab.com/mrosner/2021-research-os-OpenBSD/blob/main/installation-process-for-OpenBSD.md)
+ [command line set](https://gitlab.com/mrosner/2021-research-os-OpenBSD/blob/main/basic-command-line-set.md)

###### Thanks for your visit!
###### If you like this document, consider to share the url above or give it a star.
###### Enjoy reading.

###### 2023-12-15T18-36-58Z M.E.Rosner
